strings = input('Enter your math operation : ')
strings = strings.replace(' ', '')


def calculator(list_string_org, root_operator_list):
    if list_string_org[0] == '-' or list_string_org[0] == '+':
        a = list_string_org[0] + list_string_org[1]
        list_string_org[0] = a
        list_string_org.pop(1)
    else:
        pass
    result = int()
    for operator in root_operator_list:
        n = 0
        for index, char in enumerate(list_string_org):
            if operator == char:
                if n == 0:
                    if list_string_org.index(char) != 0 and list_string_org.index(char) != \
                            list_string_org.index(list_string_org[-1]):
                        if char == '**':
                            result = float(list_string_org[list_string_org.index(char)-1]) ** float(list_string_org[list_string_org.index(char)+1])
                            list_string_org.pop(list_string_org.index(char)-1)
                            list_string_org.pop(list_string_org.index(char)+1)
                            list_string_org[list_string_org.index(char)] = result
                            n += 1
                        elif char == '%':
                            result = float(list_string_org[list_string_org.index(char)-1]) % float(list_string_org[list_string_org.index(char)+1])
                            list_string_org.pop(list_string_org.index(char)-1)
                            list_string_org.pop(list_string_org.index(char)+1)
                            list_string_org[list_string_org.index(char)] = result
                            n += 1
                        if char == '*':
                            if list_string_org[index + 1] == '+' or list_string_org[index + 1] == '-':
                                list_string_org[index + 1] = list_string_org[index + 1] + list_string_org[index + 2]
                                list_string_org.pop(index + 2)
                                result = float(list_string_org[list_string_org.index(char) - 1]) * float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                            else:
                                result = float(list_string_org[list_string_org.index(char) - 1]) * float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                        elif char == '/':
                            if list_string_org[index + 1] == '+' or list_string_org[index + 1] == '-':
                                list_string_org[index + 1] = list_string_org[index + 1] + list_string_org[index + 2]
                                list_string_org.pop(index + 2)
                                result = float(list_string_org[list_string_org.index(char) - 1]) / float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                            else:
                                result = float(list_string_org[list_string_org.index(char) - 1]) / float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                        elif char == '-':
                            if list_string_org[index+1] == '+' or list_string_org[index+1] == '-':
                                a = list_string_org[index+1] + list_string_org[index+2]
                                list_string_org[index + 1] = list_string_org[index + 1] + list_string_org[index + 2]
                                list_string_org.pop(index + 2)
                                result = float(list_string_org[list_string_org.index(char) - 1]) - float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                            else:
                                result = float(list_string_org[list_string_org.index(char)-1]) - float(list_string_org[list_string_org.index(char)+1])
                                list_string_org.pop(list_string_org.index(char)-1)
                                list_string_org.pop(list_string_org.index(char)+1)
                                list_string_org[list_string_org.index(char)] = result
                        elif char == '+':
                            if list_string_org[index+1] == '+' or list_string_org[index+1] == '-':
                                list_string_org[index+1] = list_string_org[index+1] + list_string_org[index+2]
                                list_string_org.pop(index+2)
                                result = float(list_string_org[list_string_org.index(char) - 1]) + float(list_string_org[list_string_org.index(char) + 1])
                                list_string_org.pop(list_string_org.index(char) - 1)
                                list_string_org.pop(list_string_org.index(char) + 1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                            else:
                                result = float(list_string_org[list_string_org.index(char)-1]) + float(list_string_org[list_string_org.index(char)+1])
                                list_string_org.pop(list_string_org.index(char)-1)
                                list_string_org.pop(list_string_org.index(char)+1)
                                list_string_org[list_string_org.index(char)] = result
                                n += 1
                        else:
                            pass
                    else:
                        pass
            else:
                pass
    last_answer = float(list_string_org.pop())
    last_answer = str(last_answer)
    if last_answer[-2] == '.' and last_answer[-1] == '0':
        last_answer = int(last_answer[:-2])
    else:
        last_answer = float(last_answer)
    return last_answer


def custom_eval(list_string_org, list_operator):
    root_operator_list = []
    operator_list = []
    for i in list_operator:
        if len(operator_list) == 0:
            operator_list.append(i)
        else:
            if i == '**':
                if operator_list[-1] == '+' or operator_list[-1] == '-' or operator_list[-1] == '*' or \
                        operator_list[-1] == '/' or operator_list[-1] == '%':
                    operator_list.append(i)
                else:
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
            elif i == '%':
                if operator_list[-1] == '+' or operator_list[-1] == '-':
                    operator_list.append(i)
                else:
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
            elif i == '*':
                if operator_list[-1] == '+' or operator_list[-1] == '-':
                    operator_list.append(i)
                else:
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
            elif i == '/':
                if operator_list[-1] == '+' or operator_list[-1] == '-':
                    operator_list.append(i)
                else:
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
            elif i == '+':
                if operator_list[-1] == '*' or operator_list[-1] == '/':
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
                else:
                    operator_list.append(i)
            elif i == '-':
                if operator_list[-1] == '*' or operator_list[-1] == '/':
                    root_operator_list.append(operator_list.pop())
                    operator_list.append(i)
                else:
                    operator_list.append(i)
    root_operator = []
    for i, v in enumerate(root_operator_list):
        if v == '**' and (root_operator_list[i-1] == '/' or root_operator_list[i-1] == '*' or root_operator_list[i-1] == '%' or \
                          root_operator_list[i-1] == '+' or root_operator_list[i-1] == '-'):
            root_operator.append(root_operator_list.pop(i))
        else:
            pass
    for i, v in enumerate(operator_list):
        if v == '**' and (operator_list[i-1] == '/' or operator_list[i-1] == '*' or operator_list[i-1] == '%' or \
                          operator_list[i-1] == '+' or operator_list[i-1] == '-'):
            root_operator.append(operator_list.pop(i))
        else:
            pass
    for i, v in enumerate(operator_list):
        if v == '*' and (operator_list[i-1] == '+' or operator_list[i-1] == '-'):
            root_operator_list.append(operator_list.pop(operator_list.index(v)))
        else:
            pass
    for i, v in enumerate(operator_list):
        if v == '/' and (operator_list[i-1] == '+' or operator_list[i-1] == '-'):
            root_operator_list.append(operator_list.pop(i))
        else:
            pass
    for i, v in enumerate(operator_list):
        if v == '%' and (operator_list[i-1] == '+' or operator_list[i-1] == '-'):
            root_operator_list.append(operator_list.pop(i))
        else:
            pass
    root_operator.extend(root_operator_list)
    root_operator.extend(operator_list)
    return list_string_org, root_operator


def separate(list_string):
    list_string_org = list_string
    list_operator = []

    for item in list_string:
        if item == '*' or item == '+' or item == '-' or item == '/' or item == '%' or item == '**' or item == '%':
            index_item = list_string.index(item)
            list_operator.append(list_string[index_item])
        else:
            pass
    return list_string_org, list_operator


def chopping(string):
    list_strings = []
    for item in string:
        if item != '*' and item != '+' and item != '-' and item != '/' and item != '%':
            if len(list_strings) != 0:
                if list_strings[-1] != '*' and list_strings[-1] != '+' and list_strings[-1] != '-' and list_strings[-1] != '/' and list_strings[-1] != '%':
                    list_strings[-1] = list_strings[-1] + item
                else:
                    list_strings.append(item)
            else:
                list_strings.append(item)
        else:
            list_strings.append(item)
    for index, value in enumerate(list_strings):
        if list_strings[index] == '*':
            if list_strings[index+1] == '*':
                list_strings[index] += list_strings.pop(index+1)
    return list_strings


def braces(string):
    if ')' in string:
        index_close_braces = string.find(')')
        counter = 0
        for index in range(index_close_braces, -1, -1):
            if counter == 0 and string[index] == '(':
                index_open_braces = index
                counter += 1
        new_text = str()
        for i in range(index_open_braces + 1, index_close_braces):
            new_text += string[i]
        if ')' in new_text:
            braces(new_text)
        else:
            chopping_fun = chopping(new_text)
            separate_fun = separate(chopping_fun)
            custom_eval_fun = custom_eval(separate_fun[0], separate_fun[1])
            last_answer = calculator(custom_eval_fun[0], custom_eval_fun[1])
            global last_text
            last_text = string[:index_open_braces] + str(last_answer) + string[index_close_braces+1:]
        if ')' in string:
            braces(last_text)
        else:
            chopping(last_text)
    else:
        chopping_fun = chopping(string)
        separate_fun = separate(chopping_fun)
        custom_eval_fun = custom_eval(separate_fun[0], separate_fun[1])
        last_answer = calculator(custom_eval_fun[0], custom_eval_fun[1])

        print('\n\033[35m====================================================')
        print('\t\33[34m\33[1m * Eval answer : \t\t\t\t\t', '\033[31m', eval(strings), '\033[0m')
        print('\t\33[34m\33[1m * Last answer in custom eval : \t\033[31m ', last_answer, '\33[0m')
        print('\033[35m====================================================')


braces(strings)
